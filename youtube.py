""" YouTubeUploader class """

#!/usr/bin/python

import os
import sys
import time
import random

import httplib2

from apiclient.discovery import build
from apiclient.errors import HttpError
from apiclient.http import MediaFileUpload
from oauth2client.client import flow_from_clientsecrets
from oauth2client.file import Storage
from oauth2client.tools import argparser, run_flow

from constants import MAX_RETRIES, RETRIABLE_EXCEPTIONS, RETRIABLE_STATUS_CODES, \
    CLIENT_SECRETS_FILE, YOUTUBE_UPLOAD_SCOPE, YOUTUBE_API_SERVICE_NAME, YOUTUBE_API_VERSION, \
    MISSING_CLIENT_SECRETS_MESSAGE, VALID_PRIVACY_STATUSES

class YouTubeUploader(object):

    def __init__(self, options):
        self.file = options.file
        self.title = options.title
        self.keywords = options.keywords
        self.description = options.description
        self.category = options.category
        self.privacyStatus = options.privacyStatus
        self.options = options

    def get_authenticated_service(self):
        """
            Return the Youtube API object
        """
        flow = flow_from_clientsecrets(CLIENT_SECRETS_FILE, \
                                        scope=YOUTUBE_UPLOAD_SCOPE, \
                                        message=MISSING_CLIENT_SECRETS_MESSAGE, \
                                        redirect_uri='http://localhost:8000/')

        # Setting this will refresh the token automatically after the access token
        # expires (after an hour) 
        # http://stackoverflow.com/a/32799404/3908902
        # https://developers.google.com/identity/protocols/OAuth2WebServer#offline
        flow.params['access_type'] = 'offline'

        storage = Storage("%s-oauth2.json" % sys.argv[0])
        credentials = storage.get()

        if credentials is None or credentials.invalid:
            credentials = run_flow(flow, storage, self.options)

        return build(YOUTUBE_API_SERVICE_NAME, YOUTUBE_API_VERSION, \
                    http=credentials.authorize(httplib2.Http()))

    def initialize_upload(self, youtube):
        """
            Make request for uploading video to youtube
        """
        tags = None
        if self.keywords:
            tags = self.keywords.split(",")

        body = dict(snippet=dict(
                        title=self.title, \
                        description=self.description, \
                        tags=tags, \
                        categoryId=self.category
                    ),
                    status=dict(
                        privacyStatus=self.privacyStatus
                    )
                )

        # Call the API's videos.insert method to create and upload the video.
        insert_request = youtube.videos().insert(part=",".join(body.keys()), \
                                                  body=body, \
                                                  media_body=MediaFileUpload(
                                                        self.file, \
                                                        chunksize=-1, \
                                                        resumable=True
                                                    )
                                                )
        self.resumable_upload(insert_request)

    def resumable_upload(self, insert_request):
        """
            This method implements an exponential backoff strategy to resume a failed upload.
        """
        response = None
        error = None
        retry = 0
        while response is None:
            try:
                print "Uploading file..."
                status, response = insert_request.next_chunk()
                if 'id' in response:
                    print "Video id '%s' was successfully uploaded." % response['id']
                else:
                    exit("The upload failed with an unexpected response: %s" % response)
            except HttpError, e:
                if e.resp.status in RETRIABLE_STATUS_CODES:
                    error = "A retriable HTTP error %d occurred:\n%s" % (e.resp.status,
                                                                         e.content)
                else:
                    raise
            except RETRIABLE_EXCEPTIONS, e:
                error = "A retriable error occurred: %s" % e

            if error is not None:
                print error
                retry += 1
                if retry > MAX_RETRIES:
                    exit("No longer attempting to retry.")

                max_sleep = 2 ** retry
                sleep_seconds = random.random() * max_sleep
                print "Sleeping %f seconds and then retrying..." % sleep_seconds
                time.sleep(sleep_seconds)
