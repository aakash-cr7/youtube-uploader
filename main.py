""" The main executable script """
import os

from oauth2client.tools import argparser
from apiclient.errors import HttpError

from youtube import YouTubeUploader
from constants import VALID_PRIVACY_STATUSES

if __name__ == '__main__':
    argparser.add_argument("--file", required=True, help="Video file to upload")
    argparser.add_argument("--title", help="Video title", default="Test Title")
    argparser.add_argument("--description", help="Video description",
                            default="Test Description")
    argparser.add_argument("--category", default="22", \
                            help="Numeric video category. " +
                            "See https://developers.google.com/youtube/v3/docs/videoCategories/list")
    argparser.add_argument("--keywords", help="Video keywords, comma separated",
                            default="")
    argparser.add_argument("--privacyStatus", choices=VALID_PRIVACY_STATUSES,
                            default=VALID_PRIVACY_STATUSES[0], help="Video privacy status.")
    args = argparser.parse_args()

    if not os.path.exists(args.file):
        exit("Please specify a valid file using the --file= parameter.")

    uploader = YouTubeUploader(args)
    youtube = uploader.get_authenticated_service()
    try:
        uploader.initialize_upload(youtube)
    except HttpError, error:
        print "An HTTP error %d occurred:\n%s" % (error.resp.status, error.content)
