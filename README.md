# Youtube upload

Pyhton script to upload videos on youtube

### Usage
  - Install the requirements
  ```
    pip install -r requirements.txt
  ```
  - Add *client_id* and *client_secret* to the file *client_secrets.json* ([info](https://developers.google.com/identity/protocols/OAuth2#basicsteps))
  - Change the *redirect_uris* in the files *client_secrets.json* and *youtube.py*
  - Upload a video to youtube (By default privacyStatus: public)
  ```
  python main.py --file="/tmp/test_video_file.flv"
                 --title="Summer vacation in California"
                 --description="Had fun surfing in Santa Cruz"
                 --keywords="surfing,Santa Cruz"
                 --category="22"
                 --privacyStatus="private"
  ```

